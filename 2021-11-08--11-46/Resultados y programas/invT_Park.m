%% Funci�n para Transf. d-q a alfa-beta.
% v01: 31-07-2013

function [alfa,beta] = invT_Park(signal_a,signal_b,thetae)

%     for m=1:length(signal_b)
%         alfa(m) = signal_a*cos(thetae(m)) - signal_b*sin(thetae(m));
%         beta(m) = signal_a*sin(thetae(m)) + signal_b*cos(thetae(m));
%     end
    
    %alfa_aux = signal_a.*cos(thetae) - signal_b.*sin(thetae);
    %beta_aux = signal_a.*sin(thetae) + signal_b.*cos(thetae);
    %alfa=[0; alfa_aux(1:length(signal_b)-1)];
    %beta=[0; beta_aux(1:length(signal_b)-1)];
    alfa = signal_a.*cos(thetae) - signal_b.*sin(thetae);
    beta = signal_a.*sin(thetae) + signal_b.*cos(thetae);% un periodo de muestreo adelantado se corrije en graficos
end