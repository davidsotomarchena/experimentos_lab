/*========================================================================================================================
   EPWM Module Configuration
========================================================================================================================*/


// TBCTL (Time-Base Control)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// TBCTR MODE bits
#define TB_COUNT_UP     0x0
#define TB_COUNT_DOWN   0x1
#define TB_COUNT_UPDOWN 0x2
#define TB_FREEZE       0x3
// PHSEN bit
#define TB_DISABLE      0x0
#define TB_ENABLE       0x1
// PRDLD bit
#define TB_SHADOW       0x0
#define TB_IMMEDIATE    0x1
// SYNCOSEL bits
#define TB_SYNC_IN      0x0
#define TB_CTR_ZERO     0x1
#define TB_CTR_CMPB     0x2
#define TB_SYNC_DISABLE 0x3
// HSPCLKDIV and CLKDIV bits
#define TB_DIV1         0x0
#define TB_DIV2         0x1
#define TB_DIV4         0x2
// PHSDIR bit
#define TB_DOWN         0x0
#define TB_UP           0x1

// CTRDIR bit
#define TB_CTRDIR_DOWN  0x0
#define TB_CTRDIR_UP    0x1


// CMPCTL (Compare Control)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// LOADAMODE and LOADBMODE bits
#define CC_CTR_ZERO     0x0
#define CC_CTR_PRD      0x1
#define CC_CTR_ZERO_PRD 0x2
#define CC_LD_DISABLE   0x3
// SHDWAMODE and SHDWBMODE bits
#define CC_SHADOW       0x0
#define CC_IMMEDIATE    0x1



// AQCTLA and AQCTLB (Action-qualifier Control)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// ZRO, PRD, CAU, CAD, CBU, CBD bits
#define AQ_NO_ACTION    0x0
#define AQ_CLEAR        0x1
#define AQ_SET          0x2
#define AQ_TOGGLE       0x3



// DBCTL (Dead-Band Control)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// MODE bits
#define DB_DISABLE      0x0
#define DBA_ENABLE      0x1
#define DBB_ENABLE      0x2
#define DB_FULL_ENABLE  0x3
// POLSEL bits
#define DB_ACTV_HI      0x0
#define DB_ACTV_LOC     0x1
#define DB_ACTV_HIC     0x2
#define DB_ACTV_LO      0x3

// PCCTL (chopper control)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// CHPEN bit
#define CHP_ENABLE      0x0
#define CHP_DISABLE     0x1
// CHPFREQ bits
#define CHP_DIV1        0x0
#define CHP_DIV2        0x1
#define CHP_DIV3        0x2
#define CHP_DIV4        0x3
#define CHP_DIV5        0x4
#define CHP_DIV6        0x5
#define CHP_DIV7        0x6
#define CHP_DIV8        0x7
// CHPDUTY bits
#define CHP1_8TH        0x0
#define CHP2_8TH        0x1
#define CHP3_8TH        0x2
#define CHP4_8TH        0x3
#define CHP5_8TH        0x4
#define CHP6_8TH        0x5
#define CHP7_8TH        0x6


// TZCTL (Trip-zone Control)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// TZA and TZB bits
#define TZ_HIZ          0x0
#define TZ_FORCE_HI     0x1
#define TZ_FORCE_LO     0x2
#define TZ_DISABLE      0x3


// ETSEL (Event-trigger Select)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// INTSEL, SOCASEL, SOCBSEL bits
#define ET_CTR_ZERO     0x1
#define ET_CTR_PRD      0x2
#define ET_CTRU_CMPA    0x4
#define ET_CTRD_CMPA    0x5
#define ET_CTRU_CMPB    0x6
#define ET_CTRD_CMPB    0x7


// SOCAEN, SOCBEN, INTEN
#define ET_ETSEL_ENABLE  0x1
#define ET_ETSEL_DISABLE 0x0


// ETPS (Event-trigger Prescale)
// = = = = = = = = = = = = = = = = = = = = = = = = = =
// INTPRD, SOCAPRD, SOCBPRD bits
#define ET_DISABLE      0x0
#define ET_1ST          0x1
#define ET_2ND          0x2
#define ET_3RD          0x3
// = = = = = = = = = = = = = = = = = = = = = = = = = =




interrupt void PTC5Fepwm_isr(){                    // ePWM Interrupt Service Rutine

   timeout                               = 0;       // Clear PWM Time Out Flag

   EPwm1Regs.ETCLR.bit.INT               = 1;       // Clear EPWM1_INT Flag
   PieCtrlRegs.PIEACK.bit.ACK3           = 1;       // Clear the PIEACK of Group 3 for enables Interrupt Resquest at CPU Level
    
}// interrupt void IRFOC5Fepwm_isr()



//##################################################### EPWM5F #################################################################

void PTC5Fepwm5F_start(){
    
   EALLOW;                                          // Enable writing to EALLOW protected registers
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC     = 0;       // Stop all the TB clocks
	EDIS;                                            // Disable writing to EALLOW protected registers
    
//  Enable the 	ePWM-CLK 1-6
	EALLOW;                                          // Enable writing to EALLOW protected registers
	SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK    = 1;       // Enable ePWM1 clock
	SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK    = 1;       // Enable ePWM2 clock
	SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK    = 1;       // Enable ePWM3 clock*/
	SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK    = 1;       // Enable ePWM4 clock
	SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK    = 1;       // Enable ePWM5 clock
	EDIS;
//  End Enable the 	ePWM-CLK 1-6
    
//  Configure the ePWM1-6 shared pins
	EALLOW;                                          // Enable writing to EALLOW protected registers
	GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 1;       // Configure GPIO0 as EPWM1A
	GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 1;       // Configure GPIO1 as EPWM1B
	GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 1;       // Configure GPIO2 as EPWM2A
	GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 1;       // Configure GPIO3 as EPWM2B
	GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 1;       // Configure GPIO4 as EPWM3A
	GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 1;       // Configure GPIO5 as EPWM3B
	GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 1;       // Configure GPIO6 as EPWM4A
	GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 1;       // Configure GPIO7 as EPWM4B
	GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 1;       // Configure GPIO8 as EPWM5A
	GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 1;       // Configure GPIO9 as EPWM5B
	EDIS;                                            // Disable writing to EALLOW protected registers*/        
//  End Configure the ePWM1-5 shared pins
	
// TB of ePWM 1-5 Configure
	epwmmodule_config(&EPwm1Regs);
	epwmmodule_config(&EPwm2Regs);
	epwmmodule_config(&EPwm3Regs);
	epwmmodule_config(&EPwm4Regs);
	epwmmodule_config(&EPwm5Regs);
//  End TB Configure
	
	
//  EPWM1 Module as Master Synchronization
	EPwm1Regs.TBCTL.bit.PHSEN     = TB_DISABLE;      // Disables Phase Loading (Master Mode ePWM1)
   EPwm1Regs.TBCTL.bit.SYNCOSEL  = TB_CTR_ZERO;     // Generates EPWMxSYNC output when CTR = Zero
//  End EPWM1 Module as Master Synchronization
   
		
	
	EALLOW;                                          // Allows CPU to write to EALLOW protected registers
	PieVectTable.EPWM1_INT = &PTC5Fepwm_isr;         // ePWM1 Interrupt Rutine Address
	EDIS;															 // Disable CPU to write to EALLOW protected registers
		
    	
	EPwm1Regs.ETSEL.bit.INTSEL    = ET_CTR_PRD;      // Enable ePWM1 Module Event Genaration when CTR=PRD
	EPwm1Regs.ETPS.bit.INTPRD     = ET_1ST;          // ePWM1 Module Event Prescaler DIV1
		
	IER |= M_INT3;                                   // Enable EPWM1 CPU-PIEIER3 for INT5 (Group 3)
   PieCtrlRegs.PIEIER3.bit.INTx1 = 1;               // Enable the EPWM1_INT PIEIER3.1 to interrupt resquest sent to CPU Level
   PieCtrlRegs.PIEACK.bit.ACK3   = 0;               // Clear the PIEACK of Group 3 for enables Interrupt Resquest at CPU Level
   EPwm1Regs.ETSEL.bit.INTEN     = ET_ETSEL_ENABLE; // Enables EPWM1_INT Generation
	

}//void PTC5Fepwm5F_start()
//##################################################### END EPWM5F #################################################################







//##################################################### EPWM6F #################################################################


void PTC5Fepwm6F_start(){
    
   EALLOW;                                          // Enable writing to EALLOW protected registers
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC     = 0;       // Stop all the TB clocks
	EDIS;                                            // Disable writing to EALLOW protected registers
    
//  Enable the 	ePWM-CLK 1-6
	EALLOW;                                          // Enable writing to EALLOW protected registers
	SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK    = 1;       // Enable ePWM1 clock
	SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK    = 1;       // Enable ePWM2 clock
	SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK    = 1;       // Enable ePWM3 clock*/
	SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK    = 1;       // Enable ePWM4 clock
	SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK    = 1;       // Enable ePWM5 clock
	SysCtrlRegs.PCLKCR1.bit.EPWM6ENCLK    = 1;       // Enable ePWM6 clock*/
	EDIS;
//  End Enable the 	ePWM-CLK 1-6
    
//  Configure the ePWM1-6 shared pins
	EALLOW;                                          // Enable writing to EALLOW protected registers
	GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 1;       // Configure GPIO0 as EPWM1A
	GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 1;       // Configure GPIO1 as EPWM1B
	GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 1;       // Configure GPIO2 as EPWM2A
	GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 1;       // Configure GPIO3 as EPWM2B
	GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 1;       // Configure GPIO4 as EPWM3A
	GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 1;       // Configure GPIO5 as EPWM3B
	GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 1;       // Configure GPIO6 as EPWM4A
	GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 1;       // Configure GPIO7 as EPWM4B
	GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 1;       // Configure GPIO8 as EPWM5A
	GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 1;       // Configure GPIO9 as EPWM5B
	GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 1;       // Configure GPIO8 as EPWM6A
	GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 1;       // Configure GPIO9 as EPWM6B
	EDIS;                                            // Disable writing to EALLOW protected registers*/        
//  End Configure the ePWM1-6 shared pins
	
// TB of ePWM 1-6 Configure
	epwmmodule_config(&EPwm1Regs);
	epwmmodule_config(&EPwm2Regs);
	epwmmodule_config(&EPwm3Regs);
	epwmmodule_config(&EPwm4Regs);
	epwmmodule_config(&EPwm5Regs);
	epwmmodule_config(&EPwm6Regs);
//  End TB Configure
	
	
//  EPWM1 Module as Master Synchronization
	EPwm1Regs.TBCTL.bit.PHSEN     = TB_DISABLE;      // Disables Phase Loading (Master Mode ePWM1)
   EPwm1Regs.TBCTL.bit.SYNCOSEL  = TB_CTR_ZERO;     // Generates EPWMxSYNC output when CTR = Zero
//  End EPWM1 Module as Master Synchronization
   
		
	
	EALLOW;                                          // Allows CPU to write to EALLOW protected registers
	PieVectTable.EPWM1_INT = &PTC5Fepwm_isr;       // ePWM1 Interrupt Rutine Address
	EDIS;															 // Disable CPU to write to EALLOW protected registers	
	
    	
	EPwm1Regs.ETSEL.bit.INTSEL    = ET_CTR_PRD;      // Enable ePWM1 Module Event Genaration when CTR=PRD
	EPwm1Regs.ETPS.bit.INTPRD     = ET_1ST;          // ePWM1 Module Event Prescaler DIV1
		
	IER |= M_INT3;                                   // Enable EPWM1 CPU-PIEIER3 for INT5 (Group 3)
   PieCtrlRegs.PIEIER3.bit.INTx1 = 1;               // Enable the EPWM1_INT PIEIER3.1 to interrupt resquest sent to CPU Level
   PieCtrlRegs.PIEACK.bit.ACK3   = 0;               // Clear the PIEACK of Group 3 for enables Interrupt Resquest at CPU Level
   EPwm1Regs.ETSEL.bit.INTEN     = ET_ETSEL_ENABLE; // Enables EPWM1_INT Generation
	
}//void PTC5Fepwm6F_start()


void epwmmodule_config(volatile struct EPWM_REGS *ePWM_Regs){
    
   ePWM_Regs->TBPRD = pwm_period;                    // Set timer period of EPWMx
    
	ePWM_Regs->TBPHS.half.TBPHS     = 0;			  // Set Phase register to zero
	
	ePWM_Regs->TBCTL.bit.CTRMODE   = TB_COUNT_UPDOWN; // Up-Down-Count Mode
	ePWM_Regs->TBCTL.bit.PHSEN     = TB_ENABLE;      // Phase loading Enabled (Slave Mode)
	ePWM_Regs->TBCTL.bit.PRDLD     = TB_SHADOW;       // Period load as shadow mode
	ePWM_Regs->TBCTL.bit.SYNCOSEL  = TB_SYNC_IN;      // Selects EPWMxSYNC as sincronization output
   ePWM_Regs->TBCTL.bit.CLKDIV    = TB_DIV4;         // division by 4 
   ePWM_Regs->TBCTL.bit.HSPCLKDIV = TB_DIV1;		  // TBCLK = SYSCLKOUT / (HSPCLKDIV x CLKDIV) = 37.5MHz
   ePWM_Regs->TBCTL.bit.PHSDIR    = TB_UP;			  // Phase Direction Count-Up after the synchronization
    
    
   ePWM_Regs->CMPCTL.bit.SHDWAMODE = CC_SHADOW;       // Enable the CMPA to operate in Shadow Mode
   ePWM_Regs->CMPCTL.bit.SHDWBMODE = CC_SHADOW;       // Enable the CMPB to operate in Shadow Mode
   ePWM_Regs->CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;     // Load the CMPA when TBCTR=0
   ePWM_Regs->CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;     // Load the CMPB when TBCTR=0
    
    
   ePWM_Regs->AQCTLA.bit.CAU      = AQ_SET;          // Set the EPWMxA when TBCTR=CMPA in Up Mode
   ePWM_Regs->AQCTLA.bit.CAD      = AQ_CLEAR;        // Clear the EPWMxA when TBCTR=CMPA in Down Mode
   ePWM_Regs->AQCTLA.bit.ZRO      = AQ_CLEAR;		  // Action Counter = Zero
	

	ePWM_Regs->AQCTLA.bit.PRD      = AQ_NO_ACTION;	  // Action Counter = Period
    
    
   ePWM_Regs->DBCTL.bit.POLSEL    = DB_ACTV_HIC;     // Active High Complemertary: EPWMxB=~EPWMxA
   ePWM_Regs->DBCTL.bit.OUT_MODE  = DB_FULL_ENABLE;  // Active Dead-Band in the EPWMx Output
   ePWM_Regs->DBRED               = DEADBAND_VALUE;  // Rising-edge delay time = 1 uS (assuming TBCLK = 37.5MHz)
	ePWM_Regs->DBFED               = DEADBAND_VALUE;  // Falling-edge delay time = 1 uS (assuming TBCLK = 37.5MHz)
	
	
	ePWM_Regs->PCCTL.bit.CHPEN     = 0;				  // Disable Chopper control


	ePWM_Regs->TZCTL.bit.TZA       = TZ_DISABLE;	  // Trip-Zone A disable
	ePWM_Regs->TZCTL.bit.TZB       = TZ_DISABLE;	  // Trip-Zone B disable

	ePWM_Regs->TZEINT.bit.OST      = 0;				  // Trip-Zone A disable ON SHOT
	ePWM_Regs->TZEINT.bit.CBC      = TZ_DISABLE;      // Trip-Zone B disable CYCLE BY CYCLE

	
	ePWM_Regs->CMPA.half.CMPA      = pwm_period;      // Load de CMPA value at PRD
    

     
}//void epwmmodule_config(volatile struct EPWM_REGS *ePWM_Regs)

//##################################################### END EPWM6F #################################################################
