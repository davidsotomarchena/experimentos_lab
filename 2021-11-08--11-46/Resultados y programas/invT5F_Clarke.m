%% Funci�n para Transf. alfa-beta-x-y-z de la se�al de in.

function [ia,ib,ic,id,ie] = invT5F_Clarke(signal_alpha,signal_beta)

    %Quitar valor medio:
    signal_alpha = signal_alpha-mean(signal_alpha);
    signal_beta = signal_beta-mean(signal_beta);
    
    K = 2/5;
    alpha = 2*pi/5;
    b = 1/2;
    
    C = K*[  1  cos(alpha)   cos(2*alpha) cos(3*alpha) cos(4*alpha);
             0  sin(alpha)   sin(2*alpha) sin(3*alpha) sin(4*alpha);
             1  cos(2*alpha) cos(4*alpha) cos(alpha)   cos(3*alpha);
             0  sin(2*alpha) sin(4*alpha) sin(alpha)   sin(3*alpha);
             b       b             b            b            b    ];
         
    invC = inv(C);
    
    for m=1:length(signal_beta)
        ifase=invC*[signal_alpha(m),signal_beta(m),0,0,0]';
        ia(m)=ifase(1);
        ib(m)=ifase(2);
        ic(m)=ifase(3);
        id(m)=ifase(4);
        ie(m)=ifase(5);
    end

end