// CPUTMR0 for a step of reference wm_ref in .1 seg.

Uint16 secntr=0;
Uint16 imcntr = 0;

interrupt void PTC5Fcputmr0_isr(){ 	  

	 CpuTimer0Regs.TCR.bit.TIF   = 1;					// Reset the Cpu-Timer0 Interrupt Flag
     PieCtrlRegs.PIEACK.bit.ACK1 = 1;					// Clear the PIEACK of Group 1 for new Interrupt Resquest        

}// void PTC5Fcputmr0_isr()



void PTC5Fcputmr0_start(){     
      
    CpuTimer0Regs.PRD.all = FRECUENCIA_SYSCLKOUT/FCPUTIMER0_HZ;              // Timer Period
        
    CpuTimer0Regs.TCR.bit.FREE=1;						// CPU-TIMER0 free run
    
    EALLOW;														// Enable CPU writing to protected registers
    PieVectTable.TINT0=&PTC5Fcputmr0_isr;			// Define the CPU-TIMER0 ISR address   
    EDIS;														// Disable CPU writing to protected registers
	 
	EALLOW;                                        // Enable CPU writing to protected registers
    SysCtrlRegs.PCLKCR3.bit.CPUTIMER0ENCLK = 1;    // Enable the SYSCLKOUT to the CPU-TIMER0
    EDIS;                                          // Disable CPU writing to protected registers
    
    IER |=M_INT1;												// Enable de CPU-TIMER0 CPU-PIEIER1 for INT1 (Group 1)
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;					// Enable the CPU-TIMER0 PIEIER1.7 to interrupt resquest sent to CPU Level
    PieCtrlRegs.PIEACK.bit.ACK1	 = 0;					// Clear the PIEACK of Group 1 for enables Interrupt Resquest at CPU Level
    CpuTimer0Regs.TCR.bit.TIE	= 1;					// CPU-TIMER0 Interruption Enable

}// PTC5Fcputmr0_start()

