---
title: "Sesión de Laboratorio 8/11/2021 11:46; 10/11/2021 10:49"
author: David
---

# Planificación

1. Copiar todo el contenido relecante del disco duro del ordenador al pen drive.
Se entiende como contenido relevante:
    - Los ejecutables de los distintos programas
    - El código del DSP desarrollado por ~~investigadores anteriores~~ Cristina.
1. Se pondrá en marcha el sistema y se comprobará el correcto funcionamiento
comparado con los resultados de días anteriores un primer test. Se usarán valores
de 32 V DC en la batería. Si estos valores no proporcionasen intensidades
similares a los 2 A se subiría apropiadamente hasta llegar a ese valor. En ese caso
**se anotará en la sección de desarrollo**.
1. Se tomaran los valores de las resistencias Fase Neutro de cada fase
1. Se tomará los datos de **resistencia Fase Neutro, voltaje Fase Neutro,
intensidad de fase por osciloscopio e intensidad de fase por sensor**, para cada
una de las fases.
1. Se cerrará todo adecuadamente.

# Notas de desarrollo

El ruido en la medida tiene una media de menor que 1.8mA. Sobre los 2A esperados
supone menos de un 0.1% de error inducido por lo que se podría despreciar.

En la prueba todo ha salido correctamente. Con 16 V se han obtenido 888mA de
corriente, por lo que se predicen 1.7 A con 32. Es posible que sea necesario
hacer un ajuste.

V=-11.43V
I=0.868V

***Ajuste a 36V***

# Resultados

Los resultados obtenidos deben ser procesados adecuadamente para obtener la
calibración de los sensores.

## Resultados calibración

2^5 - 1 = 32 -1 = 31

|Fase|Posición  |Intensidad F|Voltaje F-N (V)|Carpeta|
|----|:---------|-----------:|--------------:|-------|
|E   |00001 = 01|-2.06       |27.45          |[X]   |
|E   |11110 = 30|2.04        |-27.33         |[X]   |
|D   |00010 = 02|-2.09       |27.37          |[X]   |
|D   |11101 = 29|2.04        |-27.37         |[X]   |
|C   |00100 = 04|-2.09       |27.37          |[X]   |
|C   |11011 = 27|2.05        |-27.47         |[X]   |
|B   |01000 = 08|-2.06       |27.31          |[X]   |
|B   |10111 = 23|2.08        |-27.45         |[X]   |
|A   |10000 = 16|-2.08       |27.08          |[X]   |
|A   |01111 = 15|1.99        |-27.42         |[X]   |


La pareja de la fase C carecen de sensores por lo que las medidas de los
sensores deben obtenerse restando.


Hay que tener en consideración que para obtener el valor proporcionado por el
ADC hace falta aplicar la siguiente conversión desde los valores reflejados en
el archivo.
$$I_c=-sum(I_a,I_b,I_d,I_e)$$
$$I_{inter}=(ADC-C_{offset_I})*C_{GAINFACT}$$
$$Iout=Uint16 (1638*(I_{inter}+20))$$

## Resistencias en vacío

Resistencias en vacío fase neutro.

|Fase|R_1 |R_2 |R_3 |R_4 |
|----|----|----|----|----|
|A   |12.9|12.9|12.9|12.9|
|B   |12.9|12.9|12.9|12.9|
|C   |12.8|12.8|12.8|12.8|
|D   |12.9|12.8|12.9|12.9|
|E   |13.2|13.2|13.1|13.2|
