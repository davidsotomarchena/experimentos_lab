'''Extracción de los resultados de los experimentos. Se exportan(?) a un
archivo llamado filled_model.json'''

import json
from copy import deepcopy


def process(key, value, osc, sign, vol):
    value[sign][vol]['osc'] = osc[key][sign][vol]
    path = 'datos/' + osc[key]['path'] % (
        osc[key][sign]['pos'],
        vol
    )
    if key != 'c':
        with open(path, 'r') as f:
            run_tot = 0
            for x in f:
                run_tot += int(x)
            val = run_tot/10000.0
            value[sign][vol]['sens'] = val
    else:
        files = [
            'ias.txt',
            'ibs.txt',
            'ids.txt',
            'ies.txt'
        ]
        for idx, each in enumerate(files):
            path = 'datos/' + osc[key]['path'] % (
                osc[key][sign]['pos'],
                vol
            ) + each
            with open(path, 'r') as f:
                run_tot = 0
                for x in f:
                    run_tot += int(x)
                val = run_tot/10000.0
                value[sign][vol]['sens'][idx] = val


def preprocess_osc(osc):
    '''Eliminate systemic bias'''
    osc_copy = deepcopy(osc)  # We don't want to mess with the original
    for key in osc:
        for vol in ['15', '27']:
            avr_p = osc[key]['+'][vol]
            avr_n = osc[key]['-'][vol]
            sys_err = (avr_p - avr_n)/2 - avr_p
            for sign in ['+', '-']:
                val = osc_copy[key][sign][vol]
                osc_copy[key][sign][vol] = (val + sys_err)
    return osc_copy


if __name__ == "__main__":
    '''Importar el modelo y los datos del osciloscopio'''
    with open('model.json', 'r') as f:
        experiment = json.load(f)
    with open('datos/osc.json', 'r') as f:
        osc = json.load(f)

    osc_copy = preprocess_osc(osc)

    '''For each possible experiment from each phase'''
    for key, value in experiment.items():
        for sign in ['+', '-']:
            for vol in ['15', '27']:
                process(key, value, osc_copy, sign, vol)

    '''Print everything out'''
    with open('filled_model.json', 'w') as f:
        json.dump(experiment, f, indent=4)
    with open('osc_processed.json', 'w') as f:
        json.dump(osc_copy, f, indent=4)
