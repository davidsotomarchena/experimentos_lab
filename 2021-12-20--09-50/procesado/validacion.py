import json
import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':
    np.set_printoptions(suppress=True, linewidth=800)

    with open('filled_model.json', 'r') as f:
        experiments = json.load(f)
    A = np.zeros([20, 8])
    b = np.empty([20, 1])
    line = np.zeros([16])
    ii = 0
    col = 0
    ind = 0
    for phs in experiments:
        for sgn in ['+', '-']:
            for vol in ['27', '15']:
                if phs != 'c':
                    A[ind, col] = experiments[phs][sgn][vol]['sens']
                    A[ind, col+4] = -1
                    line[ii] = experiments[phs][sgn][vol]['sens']
                    ii += 1
                else:
                    A[ind, 0:4] = -np.array(
                        experiments[phs][sgn][vol]['sens']
                    )
                    A[ind, 4:] = [1, 1, 1, 1]
                b[ind] = experiments[phs][sgn][vol]['osc']
                ind += 1
        if phs != 'c':
            col += 1

    with open('cal_1.json', 'r') as f:
        cal_ini_dict = json.load(f)
    cal_ini = np.array(cal_ini_dict['parameters']).T

    with open('cal_2.json', 'r') as f:
        cal_sq_dict = json.load(f)
    cal_sq = np.array(cal_sq_dict['parameters']).T
    cal_sq_round = np.round(cal_sq, 8)

    # dif_cals = np.abs(cal_ini - cal_sq)/np.abs(cal_ini)*100

    est_ini = A @ cal_ini
    est_sq = A @ cal_sq
    est_sq_round = A @ cal_sq_round
    error_ini = b - A @ cal_ini
    error_sq = b - A @ cal_sq
    error_sq_round = np.abs(b - A @ cal_sq_round)

    '''
    for ii in range(max(np.shape(est_ini))):
        print(
            f'${b[ii][0]:.3f}$ & '
            f'${est_ini[ii][0]:.3f}$ & ${error_ini[ii][0]:.3f}\\%$ &'
            f'${est_sq[ii][0]:.3f}$ & ${error_sq[ii][0]:.3f}\\%$ &'
            f'${est_sq_round[ii][0]:.3f}$ & ${error_sq_round[ii][0]:.3f}\\%$'
            f'\\\\'
        )
    '''

    mean_ini = np.mean(error_ini)
    mean_sq = np.mean(error_sq)
    mean_sq_round = np.mean(error_sq_round)
    '''
    print(f'${mean_ini:.3f}$ & ${mean_sq:.3f}$ & ${mean_sq_round:.3f}$ \\\\')
    '''
    mod_ini = np.var(error_ini)
    mod_sq = np.var(error_sq)
    mod_sq_round = np.var(error_sq_round)

    print(f'${mod_ini:.3f}$ & ${mod_sq:.3f}$ & ${mod_sq_round:.3f}$ \\\\')

    line = line.reshape(4, 4)
    b_no_c = np.delete(b, range(12, 16), axis=0)
    titles = ['Fase A', 'Fase B', 'Fase D', 'Fase E']
    fig, axs = plt.subplots(2, 2, figsize=(10, 10))
    axs = axs.flatten()
    for row, title in zip(range(line.shape[0]), titles):
        axs[row].plot(line[row, :], b_no_c[row * 4: row * 4 + 4], 'x')
        for cal in [cal_ini, cal_sq]:
            x = np.linspace(1900, 2270)
            y = x * cal[row] - cal[row + 4]
            axs[row].plot(x, y)
        axs[row].legend(['puntos', 'inicial', 'propuesta'])
        axs[row].set_title(title)
        axs[row].set_xlabel('Sensor')
        axs[row].set_ylabel('Interpretación')
    fig.savefig('curvas_2.eps', bbox_inches="tight")
    fig.savefig('curvas_2.svg', bbox_inches="tight")

    er_sq = error_sq_round.reshape(5, 4)
    er_ini = error_ini.reshape(5, 4)
    b = b.reshape(5, 4)
    er_sq[:, [3, 2]] = er_sq[:, [2, 3]]
    er_ini[:, [3, 2]] = er_ini[:, [2, 3]]
    b[:, [3, 2]] = b[:, [2, 3]]

    titles = ['Fase A', 'Fase B', 'Fase C', 'Fase D', 'Fase E']
    fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharex='col', sharey='all')
    axs = axs.flatten()
    for row, title in zip(range(er_sq.shape[0]), titles):
        axs[row].plot(b[row, :], np.abs(er_ini[row, :]))
        axs[row].plot(b[row, :], np.abs(er_sq[row, :]))
        axs[row].set_title(title)
        axs[row].legend(['inicial', 'propuesta'], loc='upper left')
        axs[row].set_xlabel('Corriente')
        axs[row].set_ylabel('Error')
    axs[-1].set_visible(False)
    axs[4].set_position(
        [(1-0.352)/2, 0.12, 0.352, 0.23]
    )
    fig.savefig('curvas_3.eps', bbox_inches="tight")
    fig.savefig('curvas_3.svg', bbox_inches="tight")

    ''' Figuras artículo
    Figuras modificadas para el artículo
    '''
    titles = ['Fase A', 'Fase B', 'Fase C', 'Fase D', 'Fase E']
    markers = ['b-', 'g:', 'r--', 'c-.', 'm--']
    fig, axs = plt.subplots(1, 1, figsize=(10, 10))
    for row, mark in zip(range(er_sq.shape[0]), markers):
        axs.plot(b[row, :], er_sq[row, :], mark)
    axs.set_title(title)
    axs.legend(titles, loc='upper left')
    axs.set_xlabel('Corriente')
    axs.set_ylabel('Error')
    fig.savefig('curvas_4.eps', bbox_inches="tight")
    fig.savefig('curvas_4.svg', bbox_inches="tight")

    titles = ['Fase A', 'Fase B', 'Fase C', 'Fase D', 'Fase E']
    markers = ['b-', 'g:', 'r--', 'c-.', 'm--']
    fig, axs = plt.subplots(1, 1, figsize=(10, 10))
    for row, mark in zip(range(er_sq.shape[0]), markers):
        axs.plot(b[row, :], er_ini[row, :], mark)
    axs.set_title(title)
    axs.legend(titles, loc='upper left')
    axs.set_xlabel('Corriente')
    axs.set_ylabel('Error')
    plt.show()
    fig.savefig('curvas_5.eps', bbox_inches="tight")
    fig.savefig('curvas_5.svg', bbox_inches="tight")
