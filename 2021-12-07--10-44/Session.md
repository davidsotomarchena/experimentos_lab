---
title: "Sesión de Laboratorio 7/10/12"
author: David
---

# Planificación

1. Se pondrá en marcha el sistema y se comprobará el correcto funcionamiento. Se usarán valores de 36 y 18 V DC en la batería.
1. Se tomaran los valores de las resistencias Fase Neutro de cada fase
1. Se tomará los datos de ** intensidad de fase por osciloscopio e intensidad de fase por sensor**, para cada una de las fases para cada uno de los valores de bateria, dos veces por valor y configuración
1. Se cerrará todo adecuadamente.

# Notas de desarrollo

0 en osc con -33mA Por la cara!!
# Resultados
|Fase|Posición  |Intensidad F|Voltaje Bat|Carpeta|Dir|
|----|:---------|:----------:|:---------:|:-----:|:-:|
|E   |00001 = 01|-2.11       |36         |[x]    |00 |
|E   |00001 = 01|-2.11       |36         |[x]    |01 |
|E   |00001 = 01|-1.03       |18         |[x]    |02 |
|E   |00001 = 01|-1.03       |18         |[x]    |03 |
|E   |11110 = 30|2.03        |36         |[x]    |04 |
|E   |11110 = 30|2.03        |36         |[x]    |05 |
|E   |11110 = 30|0.956       |18         |[x]    |06 |
|E   |11110 = 30| 0.956      |18         |[x]    |07 |
|D   |00010 = 02|-2.16       |36         |[x]    |08 |
|D   |00010 = 02|-2.16       |36         |[x]    |09 |
|D   |00010 = 02|-1.04       |18         |[x]    |10 |
|D   |00010 = 02|-1.05       |18         |[x]    |11 |
|D   |11101 = 29|2.10        |36         |[x]    |12 |
|D   |11101 = 29|2.09        |36         |[x]    |13 |
|D   |11101 = 29|0.975       |18         |[x]    |14 |
|D   |11101 = 29|0.978       |18         |[x]    |15 |
|C   |00100 = 04|-2.15       |36         |[x]    |16 |
|C   |00100 = 04|-2.15       |36         |[x]    |17 |
|C   |00100 = 04|-1.04       |18         |[x]    |18 |
|C   |00100 = 04|-1.04       |18         |[x]    |19 |
|C   |11011 = 27|2.09        |36         |[x]    |20 |
|C   |11011 = 27|2.09        |36         |[x]    |21 |
|C   |11011 = 27|0.978       |18         |[x]    |22 |
|C   |11011 = 27|0.980       |18         |[x]    |23 |
|B   |01000 = 08|-2.13       |36         |[x]    |24 |
|B   |01000 = 08|-2.13       |36         |[x]    |25 |
|B   |01000 = 08|-1.03       |18         |[x]    |26 |
|B   |01000 = 08|-1.03       |18         |[x]    |27 |
|B   |10111 = 23|2.08        |36         |[x]    |28 |
|B   |10111 = 23|2.08        |36         |[x]    |29 |
|B   |10111 = 23|0.974       |18         |[x]    |30 |
|B   |10111 = 23|0.975       |18         |[x]    |31 |
|A   |10000 = 16|-2.14       |36         |[x]    |32 |
|A   |10000 = 16|-2.13       |36         |[x]    |33 |
|A   |10000 = 16|-1.02       |18         |[x]    |34 |
|A   |10000 = 16|-1.02       |18         |[x]    |35 |
|A   |01111 = 15|2.07        |36         |[x]    |36 |
|A   |01111 = 15|2.08        |36         |[x]    |37 |
|A   |01111 = 15|0.965       |18         |[x]    |38 |
|A   |01111 = 15|0.959       |18         |[x]    |39 |
