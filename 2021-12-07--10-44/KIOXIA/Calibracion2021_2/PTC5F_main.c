/* ========================================================================================================================
                                      Control predictivo basdo en modelo para m�quina de 5 fases


08-12-2021, Limpieza, medida directa
==========================================================================================================================*/


#include "..\DSP28335_Regs.h"
#include "..\DSP28335_RegDef.h"
#include "..\mon28335.h"
#include "math.h"

#include "PTC5F_parameters.c"
#include "PTC5F_eqep1.c"
#include "PTC5F_epwm.c"
#include "PTC5F_adc.c"
#include "PTC5F_tmr0.c"


void main ()
	{ 
    
	int sv;
	int fds = 1;
	int sopt, vk = 0, vk_ant = 0;
    
    EALLOW;                                      // Enable writing to EALLOW protected registers
    
	SysCtrlRegs.HISPCP.bit.HSPCLK       = 0;     // HSPCLK = SYSCLKOUT / 1 = 150MHz
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;     // Stop all the TB clocks
	SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;     // Enable the SYSCLKOUT to the GPIO

	GpioCtrlRegs.GPBMUX2.bit.GPIO54		  = 0;		// Configure GPIO54 (JP3-PIN 19) as digital I/O
	GpioCtrlRegs.GPBDIR.bit.GPIO54		  = 1;		// Configure GPIO54 as digital Output (JP3-PIN 19) 
	GpioDataRegs.GPBCLEAR.bit.GPIO54		= 1;		// CLEAR the TRIGGER SIGNAL (JP3-PIN 19)
		
	
	// LED_1 Configuration
	GpioCtrlRegs.GPAMUX2.bit.GPIO16   = 0;       // Configure JP4#16(GPIO 16) as digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO16    = 1;       //Configure JP4#16(GPIO 16) as digital Output 
	GpioDataRegs.GPASET.bit.GPIO16	 = 1;       // Turn-off LED_1 
	// End LED_1 Configuration
	
	//salidas que controla rele (KSM)
	// Configuracion de J3-9
	GpioCtrlRegs.GPBMUX2.bit.GPIO48	= 0;   // Configure GPIO48 as digital I/O
	GpioCtrlRegs.GPBDIR.bit.GPIO48	= 1;		//Configure shared pins as digital Output (GPIO 48); 
	GpioDataRegs.GPBCLEAR.bit.GPIO48	= 1;		//Enciende los ventiladores

	//salidas que controla rele (KP)
	// Configuracion de J3-10
	GpioCtrlRegs.GPBMUX2.bit.GPIO49	= 0;   // Configure GPIO49 as digital I/O
	GpioCtrlRegs.GPBDIR.bit.GPIO49	= 1;		//Configure shared pins as digital Output (GPIO 49); 
	GpioDataRegs.GPBCLEAR.bit.GPIO49	= 1;		//habilita KP

	// LED_2 Configuration
	GpioCtrlRegs.GPAMUX2.bit.GPIO17   = 0;       // Configure JP4#18(GPIO 17) as digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO17    = 1;       //Configure JP4#18(GPIO 17) as digital Output 
	GpioDataRegs.GPASET.bit.GPIO17	 = 1;       // Turn-off LED_2 
	// End LED_1 Configuration

	//############################### SA_UP & SA_DOWN ##########################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO0	     = 1;       // Clear GPIO0 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO1	     = 1;       // Clear GPIO1 
	
	//####################################### SB_UP & SB_DOWN ####################################

	GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO2	     = 1;       // Clear GPIO2 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO3	     = 1;       // Clear GPIO3 
	
	//##################################### SC_UP & SC_DOWN #####################################	
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO4	     = 1;       // Clear GPIO4 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO5	     = 1;       // Clear GPIO5 
	
	//################################### SD_UP & SD_DOWN ######################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO6	     = 1;       // Clear GPIO6 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO7	     = 1;       // Clear GPIO7 
	
	//#################################### SE_UP & SE_DOWN #########################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO8	     = 1;       // Clear GPIO8 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO9	     = 1;       // Clear GPIO9 
	
	//#################################### SF_UP & SF_DOWN #########################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO10		  = 1;       // Clear GPIO10 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO11		  = 1;       // Clear GPIO11
	//###########################################################################################
	
	//#################################### TRIGGER #########################################
	GpioCtrlRegs.GPBMUX2.bit.GPIO55		  = 0;		// Configure GPIO54 (JP3-PIN 20) as digital I/O
	GpioCtrlRegs.GPBDIR.bit.GPIO55		  = 1;		// Configure GPIO54 as digital Output (JP3-PIN 20) 
	GpioDataRegs.GPBCLEAR.bit.GPIO55		= 1;		// CLEAR the TRIGGER SIGNAL (JP3-PIN 20)

	GpioCtrlRegs.GPBMUX2.bit.GPIO56		  = 0;		// Configure GPIO54 (JP3-PIN 21) as digital I/O
	GpioCtrlRegs.GPBDIR.bit.GPIO56	  = 1;		// Configure GPIO54 as digital Output (JP3-PIN 21) 
	GpioDataRegs.GPBCLEAR.bit.GPIO56		= 1;		// CLEAR the TRIGGER SIGNAL (JP3-PIN 21)

	GpioCtrlRegs.GPBMUX2.bit.GPIO57		  = 0;		// Configure GPIO54 (JP3-PIN 22) as digital I/O
	GpioCtrlRegs.GPBDIR.bit.GPIO57		  = 1;		// Configure GPIO54 as digital Output (JP3-PIN 22) 
	GpioDataRegs.GPBCLEAR.bit.GPIO57		= 1;		// CLEAR the TRIGGER SIGNAL (JP3-PIN 22)
	

	//##############################################################################################
	EDIS;                                        // Disable writing to EALLOW protected registers
   
	Fm          = FRECUENCIA_MUESTREO_HZ;        // Sampling Frequency (Hz)
    Tm          = 1/(float)Fm;                   // Sampling Period (s)
    pwm_period  = 37500000 / (2*Fm);             // timer de micro a 37.5 MHz y up/down counter


	// #################################################################################

	i_alpha = 0; 
	i_beta = 0;
	i_alpha_p1 = 0; 
	i_alpha_p2 = 0; 
	i_beta_p = 0; 
	ir_alpha_p = 0;
	ir_beta_p = 0;
	i_alpha_med = 0;
	i_beta_med = 0;
	Werror = 0;
	
	wm_k	= 0; 
	wm_km1 = 0; 
	wr = 0;
	inte_k = 0;
	inte_km1 = 0;

	Isd = Isd_ref;
	Isq = 0;



	 // ###################################### INITALITIATION ###########################################
	 
	 timeout = 0;
	 mstart  = 0;

	 
//  Start Peripherics
	
	 PTC5Fepwm5F_start();
	 PTC5Feqep_start();
	 PTC5Fadc_start();
	 PTC5Fcputmr0_start();

//	 End Start Peripherics

	// Hist�ricos
	for (sv = 0; sv<CANTIDAD_LOG; sv++ )
	{
		ias[sv] = 0;
		ibs[sv] = 0;
		ids[sv] = 0;
		ies[sv] = 0;
		ids_m[sv] = 0;
		iqs_m[sv] = 0;
		wmech1[sv] = 0;
		//we_m[sv] = 0;
		svopt[sv] = 0;  //A�ADIDO POR MI
		//ias_p1[sv] = 0;  //A�ADIDO POR MI
		//ias_p2[sv] = 0;  //A�ADIDO POR MI
	}
		
   EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 0;						// Stop all the TB clocks
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC   = 1;						// Start all the TB clocks
   EDIS; 
    
   EINT;																		// Enable interrupts to CPU-Level

	CpuTimer0Regs.TCR.bit.TRB = 1;									// Reload CPU-TIMER0 Period


	GpioDataRegs.GPBSET.bit.GPIO49	= 1;
	
	wm_ref = 0.5*wnom;

    // C�lculo de matrices dependientes de wr. Se supone wr = cte
	wr = P*wm_ref;

    while(!stop)
		{
        TRIGGER_TOGGLE_54;

		 ret_val_mon = (*callmon28335)(); // Arrancar 28335. MSK Execute Control
			GpioDataRegs.GPBSET.bit.GPIO48	= 1; //ventiladores encendidos
		
		TRIGGER_TOGGLE_55;
		while(timeout) // Stand by the timeout Flag cleared in the EPWM1 interrupt
		{
        }
		TRIGGER_TOGGLE_55;

        timeout = 1;														// Set PWM Time Out Flag 

	
	// Deshabilitaci�n Kp de fase, en_fallo, se�al ctrl m�quina DC fallo
	//===========================================================================
		if (mstart2 != 0)
		{
			contfallo++;
			if (contfallo == 2.0*FRECUENCIA_MUESTREO_HZ)
			{
				contfallo = 0;
				mstart=1;
				mstart2=0;
				logger = 0;
			}
		}


//----------------- Med. y  acondicionamiento corrientes de fase - Trans. alfa-beta-x-y ------------------*/
            //TRIGGER_TOGGLE_56;
		// ADC converter SEQ 1 is Busy
		while(AdcRegs.ADCST.bit.SEQ1_BSY)
		{						
			LED2_TOGGLE;
		}
			
			Ia_medido = AdcMirror.ADCRESULT0;
			Ib_medido = AdcMirror.ADCRESULT1;
			Id_medido = AdcMirror.ADCRESULT2; 
			Ie_medido = AdcMirror.ADCRESULT3;
			Ic_medido = -Ia_medido -Ib_medido -Id_medido -Ie_medido;
			sopt = 15;  // para meter Vdc por fase a y 0 en resto


				
				//TRIGGER_TOGGLE_57;
				//------------------------------- Generaci�n de disparos ----------------------------------*/
				EPwm1Regs.CMPA.half.CMPA = (1-SCMP[sopt][0])*pwm_period;    // Load EPWM1 CMPA
				EPwm2Regs.CMPA.half.CMPA = (1-SCMP[sopt][1])*pwm_period;    // Load EPWM2 CMPA
				EPwm3Regs.CMPA.half.CMPA = (1-SCMP[sopt][2])*pwm_period;    // Load EPWM3 CMPA
				EPwm4Regs.CMPA.half.CMPA = (1-SCMP[sopt][3])*pwm_period;    // Load EPWM4 CMPA
				EPwm5Regs.CMPA.half.CMPA = (1-SCMP[sopt][4])*pwm_period;    // Load EPWM5 CMPA
				//------------------------------- End Generaci�n de disparos -------------------------------*/
				//TRIGGER_TOGGLE_57;
				
				
				Tcarga = CCarga*(Isd*Isq_par);
				Tcarga_pre=Tcarga;

				// Transf. de las corrientes de medida a Isd-Isq:
				Id_med =  i_alpha_med*costhetae + i_beta_med*sinthetae;
				Iq_med = -i_alpha_med*sinthetae + i_beta_med*costhetae;



if ((dsampled >=DOWNSAMP)&&(mstart))
			{
				fds = 1;
				dsampled=0;
			}
else
		   
	            dsampled++;
			
//Medida decimada:

				if ((logger < CANTIDAD_LOG)&&(fds)&&(mstart)) // (mstart2==1)
				{
					ias[logger] = (Uint16)Ia_medido;
					ibs[logger] = (Uint16)Ib_medido;
					ids[logger] = (Uint16)Id_medido;
					ies[logger] = (Uint16)Ie_medido;

					logger++;
					fds=0;
					
	      		
					
		   }	
//TRIGGER_TOGGLE_54;

	}// while(!stop)

	DINT;

	EALLOW;                                          // Enable writing to EALLOW protected registers

	GpioCtrlRegs.GPAMUX1.bit.GPIO0        = 0;       // Configure GPIO0 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO0         = 1;       // Configure GPIO0 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO0	     = 1;       // Clear GPIO0 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO1        = 0;       // Configure GPIO1 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO1         = 1;       // Configure GPIO1 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO1	     = 1;       // Clear GPIO1 
	//###########################################################################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO2        = 0;       // Configure GPIO2 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO2         = 1;       // Configure GPIO2 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO2	     = 1;       // Clear GPIO2 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO3        = 0;       // Configure GPIO3 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO3         = 1;       // Configure GPIO3 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO3	     = 1;       // Clear GPIO3 
	//###########################################################################################	
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO4        = 0;       // Configure GPIO4 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO4         = 1;       // Configure GPIO4 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO4	     = 1;       // Clear GPIO4 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO5        = 0;       // Configure GPIO5 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO5         = 1;       // Configure GPIO5 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO5	     = 1;       // Clear GPIO5 
	//###########################################################################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO6        = 0;       // Configure GPIO6 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO6         = 1;       // Configure GPIO6 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO6	     = 1;       // Clear GPIO6 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO7        = 0;       // Configure GPIO7 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO7         = 1;       // Configure GPIO7 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO7	     = 1;       // Clear GPIO7 
	//###########################################################################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO8        = 0;       // Configure GPIO8 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO8         = 1;       // Configure GPIO8 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO8	     = 1;       // Clear GPIO8 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO9        = 0;       // Configure GPIO9 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO9         = 1;       // Configure GPIO9 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO9	     = 1;       // Clear GPIO9 
	//###########################################################################################
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO10       = 0;       // Configure GPIO10 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO10        = 1;       // Configure GPIO10 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO10		  = 1;       // Clear GPIO10 
	
	GpioCtrlRegs.GPAMUX1.bit.GPIO11       = 0;       // Configure GPIO11 as Digital I/O
	GpioCtrlRegs.GPADIR.bit.GPIO11        = 1;       // Configure GPIO11 as digital Output 
	GpioDataRegs.GPACLEAR.bit.GPIO11	     = 1;       // Clear GPIO11
	//###########################################################################################
	                              // Enable writing to EALLOW protected registers
	SysCtrlRegs.WDCR = 0x000F;            // Enable Watchdog and write incorrect WD Check Bits
	asm(" NOP");
	asm(" NOP");
	asm(" NOP");   
}// void main ()
//##########################################################################################################3

