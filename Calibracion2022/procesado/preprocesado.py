import json
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt


def preprocess(data_loc):
    ''' Acquire all data of the experiments this module is common to
    calibration and validation
    '''

    expers = [exper for exper in data_loc.iterdir() if exper.is_dir()]
    currents = ['ias.txt', 'ibs.txt', 'ids.txt', 'ies.txt']
    osc_phases = ['a', 'b', 'c', 'd']

    experiments = {
        'sens': np.empty((5, len(expers))),
        'osc': np.empty((5, len(expers))),
    }

    for num, exper in enumerate(expers):

        # getting the sensors mean values of all experiments
        for ph, curr in enumerate(currents):
            with open(exper / curr) as f:
                run_tot = 0
                for line, sens in enumerate(f):
                    run_tot += int(sens)
                val = run_tot/(line + 1)
                experiments['sens'][ph][num] = val - 2100
        # Adding a one for the offset term
        experiments['sens'][ph + 1][num] = 1

        # getting the osciloscope values of all experiments
        with open(exper / 'data.json') as f:
            osc = json.load(f)
            e_phase = 0
            for ph, phase in enumerate(osc_phases):
                experiments['osc'][ph][num] = osc[phase]
                e_phase -= osc[phase]
            # Only four probes
            # The e phase is inferred form the others with ∑i=0
            experiments['osc'][ph+1][num] = e_phase

    print(experiments['osc'].mean(1, keepdims=True))
    ''' Eliminate systemic bias by eliminating means. If your sampling
    shouldn't be centered around 0 comment the line bellow
    experiments['osc'] = (experiments['osc']
                          - experiments['osc'].mean(1, keepdims=True))
    '''
    return experiments


if __name__ == '__main__':
    data1 = Path('datos')
    experiments = preprocess(data1)
    # print(experiments)

    fig, axs = plt.subplots(1, 1, figsize=(20, 10))
    fig2, axs2 = plt.subplots(1, 1, figsize=(10, 10))
    for each in range(5):
        osc = experiments['osc'][each] + each * 2
        axs.plot(osc, '-')
        idx = each if each < 4 else 0
        mul = each if each < 2 else each + 1 if each < 4 else 0
        sens = (experiments['sens'][idx] - 2100)/100 + mul * 2
        axs.plot(sens, '-')
    fig.savefig('prel1.png', bbox_inches="tight")

    data2 = Path('datos_2')
    experiments = preprocess(data2)

    fig, axs = plt.subplots(1, 1, figsize=(20, 10))
    fig2, axs2 = plt.subplots(1, 1, figsize=(10, 10))
    for each in range(5):
        osc = experiments['osc'][each] + each * 2
        axs.plot(osc, '-')
        idx = each if each < 4 else 0
        mul = each if each < 2 else each + 1 if each < 4 else 0
        sens = (experiments['sens'][idx] - 2100)/100 + mul * 2
        axs.plot(sens, '-')
    fig.savefig('prel2.png', bbox_inches="tight")
