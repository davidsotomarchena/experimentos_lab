from calibrado import calibration
from preprocesado import preprocess
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path


if __name__ == '__main__':
    model = preprocess(Path('datos_2'))
    c1, c2, c3, _, _ = calibration()
    s = model['sens']
    i = model['osc']

    i_hat1 = c1 @ s
    i_hat2 = c2 @ s
    i_hat3 = c3 @ s

    e1 = np.abs(i - i_hat1)
    e2 = np.abs(i - i_hat2)
    e3 = np.abs(i - i_hat3)

    titles = ['Fase A', 'Fase B', 'Fase C', 'Fase D', 'Fase E']
    fig, axs = plt.subplots(3, 2, figsize=(10, 10), sharex='col', sharey='all')
    axs = axs.flatten()
    for row, title in enumerate(titles):
        axs[row].plot(i[row], e1[row], 'x')
        axs[row].plot(i[row], e2[row], 'o')
        axs[row].plot(i[row], e3[row], '^')
        axs[row].legend(['c1', 'c2', 'c3'])
        axs[row].set_title(title)
        axs[row].set_ylabel('Error')
        axs[row].set_xlabel('Corriente real')
    axs[-1].set_visible(False)
    axs[4].set_position(
        [(1-0.352)/2, 0.12, 0.352, 0.23]
    )
    fig.savefig('curvas_e_i.eps', bbox_inches="tight")
    fig.savefig('curvas_e_i.svg', bbox_inches="tight")

    m1 = np.mean(e1, axis=1)
    m2 = np.mean(e2, axis=1)
    m3 = np.mean(e3, axis=1)
    v1 = np.var(e1, axis=1)
    v2 = np.var(e2, axis=1)
    v3 = np.var(e3, axis=1)

    print('Raw means and variance of errors')
    print(m1, '\n', v1, '\n')
    print(m2, '\n', v2, '\n')
    print(m3, '\n', v3, '\n')

    print('Means, lower is better, Variances, closer to 0 is better')
    im1 = np.abs(m1/m1)
    im2 = np.abs(m2/m1)
    im3 = np.abs(m3/m1)

    vim1 = np.abs(v1/v1)
    vim2 = np.abs(v2/v1)
    vim3 = np.abs(v3/v1)

    print('Lower is better')
    print(im1, '\n', vim1, '\n')
    print(im2, '\n', vim2, '\n')
    print(im3, '\n', vim3, '\n')
